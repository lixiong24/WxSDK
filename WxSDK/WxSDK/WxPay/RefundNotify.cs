﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI;
using WxSDK.Common;
using WxSDK.Config;

namespace WxSDK.WxPay
{
	/// <summary>
	/// 申请退款通知回调处理类
	/// </summary>
	public class RefundNotify : Notify
	{
		/// <summary>
		/// 
		/// </summary>
		/// <param name="page"></param>
		public RefundNotify(Page page) : base(page)
		{
		}

		/// <summary>
		/// 得到返回的解密数据
		/// </summary>
		/// <param name="req_info"></param>
		/// <returns></returns>
		public WxData GetReqInfo(string req_info)
		{
			if (string.IsNullOrEmpty(req_info))
			{
				return null;
			}
			WxData resultData = new WxData();
			string strKey = WxUtility.MD5(WxConfig.KEY);
			string strInfo = WxUtility.AESDecrypt(strKey, req_info);
			resultData.FromXml(strInfo, false);
			return resultData;
		}
	}
}
