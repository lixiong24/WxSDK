﻿using LitJson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WxSDK.Common;

namespace WxSDK.WxLive
{
	/// <summary>
	/// 直播API
	/// </summary>
	public class WxLiveApi
	{
		/// <summary>
		/// 创建直播间
		/// </summary>
		/// <param name="access_token"></param>
		/// <param name="wxData"></param>
		/// <returns></returns>
		public static WxData CreateRoom(string access_token,WxData wxData)
		{
			if (string.IsNullOrEmpty(access_token))
			{
				throw new WxException("access_token不能为空");
			}
			if (!wxData.IsSet("name") || wxData.GetValue("name").ToString() == "")
			{
				throw new WxException("直播间名字不能为空");
			}
			if (!wxData.IsSet("coverImg") || wxData.GetValue("coverImg").ToString() == "")
			{
				throw new WxException("背景图不能为空");
			}
			if (!wxData.IsSet("startTime") || wxData.GetValue("startTime").ToString() == "")
			{
				throw new WxException("直播计划开始时间不能为空");
			}
			if (!wxData.IsSet("endTime") || wxData.GetValue("endTime").ToString() == "")
			{
				throw new WxException("直播计划结束时间不能为空");
			}
			if (!wxData.IsSet("anchorName") || wxData.GetValue("anchorName").ToString() == "")
			{
				throw new WxException("主播昵称不能为空");
			}
			if (!wxData.IsSet("anchorWechat") || wxData.GetValue("anchorWechat").ToString() == "")
			{
				throw new WxException("主播微信号不能为空");
			}
			if (!wxData.IsSet("shareImg") || wxData.GetValue("shareImg").ToString() == "")
			{
				throw new WxException("分享图不能为空");
			}
			if (!wxData.IsSet("feedsImg") || wxData.GetValue("feedsImg").ToString() == "")
			{
				throw new WxException("购物直播频道封面图不能为空");
			}
			if (!wxData.IsSet("type") || wxData.GetValue("type").ToString() == "")
			{
				throw new WxException("直播间类型不能为空");
			}
			if (!wxData.IsSet("closeLike") || wxData.GetValue("closeLike").ToString() == "")
			{
				throw new WxException("是否关闭点赞不能为空");
			}
			if (!wxData.IsSet("closeGoods") || wxData.GetValue("closeGoods").ToString() == "")
			{
				throw new WxException("是否关闭货架不能为空");
			}
			if (!wxData.IsSet("closeComment") || wxData.GetValue("closeComment").ToString() == "")
			{
				throw new WxException("是否关闭评论不能为空");
			}
			string strUrl = "https://api.weixin.qq.com/wxaapi/broadcast/room/create?access_token=" + access_token;
			string param = wxData.ToJson();
			Log.Debug("创建直播间", "请求地址：" + strUrl + " 参数：" + param);
			var response = HttpService.HttpPostByJSON(strUrl, param);
			if (response.Contains("errcode"))
			{
				Log.Debug("创建直播间", "返回数据：" + response);
			}
			JsonData jd = JsonMapper.ToObject(response);
			var errcode = (string)jd["errcode"];
			var roomId = (string)jd["roomId"];
			var qrcode_url = (string)jd["qrcode_url"];
			WxData result = new WxData();
			result.SetValue("errcode", errcode);
			result.SetValue("roomId", roomId);
			result.SetValue("qrcode_url", qrcode_url);
			return result;
		}

		/// <summary>
		/// 获取直播间列表
		/// </summary>
		/// <param name="access_token"></param>
		/// <param name="start">起始房间，0表示从第1个房间开始拉取</param>
		/// <param name="limit">每次拉取的房间数量，建议100以内</param>
		/// <returns></returns>
		public static string GetRoomList(string access_token,int start = 0,int limit = 100)
		{
			if (string.IsNullOrEmpty(access_token))
			{
				throw new WxException("access_token不能为空");
			}
			string strUrl = "https://api.weixin.qq.com/wxa/business/getliveinfo?access_token=" + access_token;
			WxData wxData = new WxData();
			wxData.SetValue("start", start);
			wxData.SetValue("limit", limit);
			string param = wxData.ToJson();
			Log.Debug("获取直播间列表", "请求地址：" + strUrl + " 参数：" + param);
			var response = HttpService.HttpPostByJSON(strUrl, param);
			Log.Debug("获取直播间列表", "返回数据：" + response);
			return response;
		}

		/// <summary>
		/// 获取直播间回放列表
		/// </summary>
		/// <param name="access_token"></param>
		/// <param name="roomID">直播间ID</param>
		/// <param name="start">起始拉取视频，0表示从第一个视频片段开始拉取</param>
		/// <param name="limit">每次拉取的数量，建议100以内</param>
		/// <returns></returns>
		public static string GetRoomReplayList(string access_token,int roomID, int start = 0, int limit = 100)
		{
			if (string.IsNullOrEmpty(access_token))
			{
				throw new WxException("access_token不能为空");
			}
			if (roomID <= 0)
			{
				throw new WxException("房间ID不能为空");
			}
			string strUrl = "https://api.weixin.qq.com/wxa/business/getliveinfo?access_token=" + access_token;
			WxData wxData = new WxData();
			wxData.SetValue("action", "get_replay");
			wxData.SetValue("room_id", roomID);
			wxData.SetValue("start", start);
			wxData.SetValue("limit", limit);
			string param = wxData.ToJson();
			Log.Debug("获取直播间回放列表", "请求地址：" + strUrl + " 参数：" + param);
			var response = HttpService.HttpPostByJSON(strUrl, param);
			Log.Debug("获取直播间回放列表", "返回数据：" + response);
			return response;
		}

	}
}
